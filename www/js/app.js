// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            if (window.cordova && window.cordova.plugins.Keyboard) {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                // Don't remove this line unless you know what you are doing. It stops the viewport
                // from snapping when text inputs are focused. Ionic handles this internally for
                // a much nicer keyboard experience.
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
    })
    .controller('MainController', function ($scope) {
        $scope.open = function (result) {
            var table = [
                    {
                        name: 'Win',
                        start: 0,
                        end: 60
                    },
                    {
                        name: 'Loose',
                        start: 60,
                        end: 120
                    },
                    {
                        name: 'Win',
                        start: 120,
                        end: 180
                    },
                    {
                        name: 'Loose',
                        start: 180,
                        end: 240
                    },
                    {
                        name: 'Win',
                        start: 240,
                        end: 300
                    },
                    {
                        name: 'Loose',
                        start: 300,
                        end: 360
                    }
                ],
                options = {
                    table: table,
                    target: 'Win',
                    title: 'Bzzz',
                    description: 'Some description',
                    boardUrl: 'http://3cam.ru/board.png?1',
                    bottleUrl: 'http://3cam.ru/bottle.png'
                };

            function weHaveAWinner(result) {
                console.log('We have a winner: ' + result);
                alert('We have a winner: ' + result);
            }

            function pluginCallCompleted() {
                console.log('Game started');
            }

            function pluginCallFailed(reason) {
                console.log('Game call failed');
                console.error(reason);
            }

            RollerGame.startGame(options, weHaveAWinner, pluginCallCompleted, pluginCallFailed);
        };
    });
